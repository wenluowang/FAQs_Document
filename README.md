```plantuml

    participant Terminal
    participant App
    App->>Terminal: pos.doTarde()
    Terminal-->>App: OnDoTradeResult: <br/>MSR/NFC/ICC
    Note right of App: Case: MSR/NFC <br/>Send to Server -->
    App->>Terminal: Case: ICC  pos.startEmv(start)


    Note left of App: Case: Application Selection
    Terminal-->>App: OnRequestOnlineProcess  <br/> -ICC data <br/> -pinBlock
    Note left of PaymentHost: ICC Data <br/>Send to Server -->
    PaymentHost->>App: Issuer Reponse Result: <br/> Approval: 3030  <br/> Declined: 3035
    App->>Terminal: Send Issuer Result to card <br/> pos.sendOnlineProcessResult()



    Terminal-->>App: OnRequestBatchData
    
```


```plantuml
@startmindmap
* root node
** some first level node
***_ second level node
***_ another second level node
***_ foo
***_ bar
***_ foobar
** another first level node
@endmindmap
```


# Transaction API

## Transaction
|  Function           |            callback | Description |
|        :---:        | :---:               | :---:         |
| pos.DoTrade()       |     onDoTradeResult()   | swipe card need input pin        |
| pos.DoCheckCard()   |     onDoTradeResult()   | swipe no pin, then call getPin() according to card service code        |

## update key
|  Function                 |            callback       | Description |
|        :---:              | :---:                     | :---:         |
| pos.updateWorkKey()       | onUpdateWorkKeyResult()   | true: success. otherwise : failed       |

```mermaid
sequenceDiagram
    participant Terminal
    participant App
    participant PaymentHost

    Note right of PaymentHost: server use terminal <br/>TMK to encrypt IPEK <br/>(11111....)
    Terminal-->>App: OnRequestBatchData

```
